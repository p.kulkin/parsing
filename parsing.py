import openpyxl

def parse_ip_file(nomer_prohoda):
    # Открываем файл ip.xlsx
    workbook = openpyxl.load_workbook('ip.xlsx')

    # Выбираем первый лист
    sheet = workbook.worksheets[0]

    # Создаем списки m1, m2 и m3
    m1 = []
    m2 = []
    m3 = []

    # Парсим файл и выполняем необходимые действия
    for row in sheet.iter_rows():
        for cell in row:
            # Проверяем, есть ли значение переменной nomer_prohoda в текущей ячейке
            cell_value = cell.value
            if cell_value and nomer_prohoda in str(cell_value):
                # Получаем значение столбца D, разделяем его по пробелам и добавляем в список m1
                column_d_value = sheet.cell(row=cell.row, column=4).value
                if column_d_value:
                    m1.extend(str(column_d_value).split())

                # Получаем значение столбца F, разделяем его по пробелам, удаляем запятые и добавляем в список m2
                column_f_value = sheet.cell(row=cell.row, column=6).value
                if column_f_value:
                    column_f_value = column_f_value.replace(',', '')
                    m2.extend(column_f_value.split())

                # Получаем значение столбца B, разделяем его по пробелам, удаляем пустые строки и добавляем в список m3
                column_b_value = sheet.cell(row=cell.row, column=2).value
                if column_b_value:
                    m3.extend(filter(None, str(column_b_value).split()))

    # Записываем значения m1, m2 и m3 в файл output.txt
    with open('output.txt', 'w') as file:
        file.write('\n'.join(m1) + '\n')
        file.write('\n'.join(m2) + '\n')
        file.write('\n'.join(m3) + '\n')

    return m1, m2, m3

def parse_m3(m3):
    # Открываем файл ip.xlsx
    workbook = openpyxl.load_workbook('ip.xlsx')

    # Выбираем второй лист
    sheet = workbook.worksheets[1]

    # Создаем список m4
    m4 = []

    # Парсим файл и выполняем необходимые действия
    for row in sheet.iter_rows():
        for cell in row:
            # Проверяем, есть ли значение из списка m3 в текущей ячейке
            cell_value = cell.value
            if cell_value and any(value in str(cell_value) for value in m3):
                # Получаем значение столбца B, разделяем его по пробелам и добавляем в список m4
                column_b_value = sheet.cell(row=cell.row, column=2).value
                if column_b_value:
                    m4.extend(str(column_b_value).split())

    # Записываем значения m4 в файл output.txt
    with open('output.txt', 'a') as file:
        file.write('\n'.join(m4) + '\n')

    return m4

nomer_prohoda = input("Введите значение переменной nomer_prohoda: ")

m1, m2, m3 = parse_ip_file(nomer_prohoda)
m4 = parse_m3(m3)

print("m1:", m1)
print("m2:", m2)
print("m3:", m3)
print("m4:", m4)
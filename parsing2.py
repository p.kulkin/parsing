import openpyxl


def parse_file(server):
 workbook = openpyxl.load_workbook('ip.xlsx')

 ws2 = workbook.worksheets[1]  # второй лист

 m4 = []

 for row in ws2.iter_rows(min_row=2, values_only=True):
  if str(row[0]) == server:
   if row[1]:  # B-столбец
    m4 += row[1].split()

 # Вывод значений m4
 print(f"m4: {m4}")


# Ипользование функции:
parse_file('servera-a')
import openpyxl
import re


def parse_file(nomer):
 # Загрузка файла Excel
 workbook = openpyxl.load_workbook('ip.xlsx')

 ws1 = workbook.active  # Первый лист
 ws2 = workbook['Лист2']  # Допустим, второй лист называется 'Sheet2'

 m1, m2, m3 = [], [], []
 m4 = []
 pat = re.compile(r',')  # Регулярное выражение для знака ','

 # Перебор по строкам в первом листе
 for row in ws1.iter_rows(min_row=2, values_only=True):
  if str(row[0]) == nomer:
   if row[3]:  # D-столбец
    m1 += row[3].split()
   if row[5]:  # F-столбец
    parsed_values = pat.sub('', row[5]).split()
    m2 += parsed_values
   if row[1]:  # B-столбец
    m3 += list(filter(None, row[1].split()))  # Удаление пустых строк при помощи filter

 # Перебор строк второго листа
 for row in ws2.iter_rows(min_row=2, values_only=True):
  if row[0] in m3:
   if row[1]:
    m4 += row[1].split()

 # Вывести значения m1, m2, m3, m4
 print(f"m1 values: {', '.join(m1)}")
 print(f"m2 values: {', '.join(m2)}")
 print(f"m3 values: {', '.join(m3)}")
 print(f"m4 values: {', '.join(m4)}")


# Пример использования функции
parse_file('1.5')